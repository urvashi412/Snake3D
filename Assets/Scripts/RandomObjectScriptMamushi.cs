﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectScriptMamushi : RandomObjectsScriptBase {

	public override GameObject InstantiateRandomObject(){

		int randomIndex=Random.Range(0,items.Length);

		//items[randomIndex].AddComponent<ItemScript>();
		Vector3 instantiationLoc=new Vector3(Random.Range(minx,maxx),0,Random.Range(minz,maxz));

		GameObject item=Instantiate(items[randomIndex],instantiationLoc,Quaternion.identity);

		item.tag="Fruit";
		Destroy(item.GetComponent<ItemBaseScript>());
		item.AddComponent<ItemScriptWithPoison>();
		return item;
	}
}
