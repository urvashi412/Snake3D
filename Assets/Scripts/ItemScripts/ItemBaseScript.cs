﻿using UnityEngine;
using System.Collections;

public class ItemBaseScript:MonoBehaviour {


	SnakePlayerBase snakeMovement;
	GameManagerScript gameManagerScript;
	public virtual void Start(){
		snakeMovement=(SnakePlayerBase)FindObjectOfType(typeof(SnakePlayerBase));
		gameManagerScript=GameObject.Find("GameManager").GetComponent<GameManagerScript>();
	}
	public virtual void  OnTriggerEnter(Collider other){

		if(other.tag=="PlayerHead")
		{


			snakeMovement.Eat();
			gameManagerScript.updateScore(1);
			Destroy(gameObject);
		}

	}
}
