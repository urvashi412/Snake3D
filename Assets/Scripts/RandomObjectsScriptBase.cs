﻿using UnityEngine;
using System.Collections;

public class RandomObjectsScriptBase : MonoBehaviour {

	public GameObject[] items;
	public GameObject minX,minZ,maxX,maxZ;
	public float minx,maxx,minz,maxz;
	public bool isWaiting=false;
	public bool gameStarted=false;
	SingletonScript singletonScript;
	void Start () {
		singletonScript=GameObject.Find("Singleton").GetComponent<SingletonScript>();
		minx=minX.transform.position.x;
		maxx=maxX.transform.position.x;
		minz=minZ.transform.position.z;
		maxz=maxZ.transform.position.z;
	}
	


	void Update () {
		if(gameStarted==false)
		gameStarted=singletonScript.gameStarted;
		if(!isWaiting &&gameStarted){
			GameObject newItem=InstantiateRandomObject();
			isWaiting=true;
			Hashtable table=new Hashtable();
			table.Add("amount",5);table.Add("time",4);
			Vector3 newSize=new Vector3(newItem.transform.localScale.x+0.5f,newItem.transform.localScale.y+0.5f,newItem.transform.localScale.z+0.5f);
			iTween.PunchPosition(newItem,newSize,3);
			StartCoroutine(WaitForSeconds(3));
		}

	}

	public virtual GameObject InstantiateRandomObject(){

		int randomIndex=Random.Range(0,items.Length);

		//items[randomIndex].AddComponent<ItemScript>();
		Vector3 instantiationLoc=new Vector3(Random.Range(minx,maxx),0,Random.Range(minz,maxz));

		GameObject obj=Instantiate(items[randomIndex],instantiationLoc,Quaternion.identity);
		return obj;

	}
	IEnumerator WaitForSeconds(int sec){

		yield return new WaitForSeconds(3);
		isWaiting=false;
	}
}
