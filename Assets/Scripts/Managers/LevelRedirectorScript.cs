﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelRedirectorScript : MonoBehaviour {
	

	public void StartClassic(){
		PlayerPrefs.SetString("SceneToLoad","Classic");
		SceneManager.LoadScene(1);
	}
	public void StartPassage(){
		PlayerPrefs.SetString("SceneToLoad","Passage");
		SceneManager.LoadScene(1);
	}
	public void StartMamushi(){
		PlayerPrefs.SetString("SceneToLoad","Mamushi");
		SceneManager.LoadScene(1);
	}
	public void StartOuroboro(){
		PlayerPrefs.SetString("SceneToLoad","Ouroboro");
		SceneManager.LoadScene(1);
	}
	public void StartCobra(){
		PlayerPrefs.SetString("SceneToLoad","Cobra");
		SceneManager.LoadScene(1);
	}
}
