﻿using UnityEngine;
using System.Collections;

public class ItemScriptWithPoison : ItemBaseScript {


	SnakePlayerBase snakeMovement;
	GameManagerScript gameManagerScript;
	public Material poisonMat;
	public override void Start(){
		snakeMovement=(SnakePlayerBase)FindObjectOfType(typeof(SnakePlayerBase));
		gameManagerScript=GameObject.Find("GameManager").GetComponent<GameManagerScript>();
		poisonMat=Resources.Load("Material/PoisonFruit",typeof(Material)) as Material;
	}
	public override void OnTriggerEnter(Collider other){
		/*if(other.tag=="PlayerHead" && gameObject.tag=="Poison")
		{

		
			Debug.Log("Game over!!");
		}
		else*/ if(other.tag=="PlayerHead" && gameObject.tag=="Fruit")
		{

			snakeMovement.Eat();
			gameManagerScript.updateScore(1);
			
				gameObject.GetComponent<Renderer> ().material = poisonMat;

			StartCoroutine(ChangeTag());


		}
		 
	}
	IEnumerator ChangeTag(){
		yield return new WaitForSeconds(2);
		gameObject.tag="Poison";
	}
}
