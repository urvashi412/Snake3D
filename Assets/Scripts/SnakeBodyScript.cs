﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SnakeBodyScript : MonoBehaviour {

	SnakePlayerBase snakePlayer;
	GameManagerScript gameManagerScript;
	SingletonScript singletonScript;
	void Start () {
		singletonScript=GameObject.Find("Singleton").GetComponent<SingletonScript>();
		snakePlayer=(SnakePlayerBase)FindObjectOfType(typeof(SnakePlayerBase));
		gameManagerScript=GameObject.Find("GameManager").GetComponent<GameManagerScript>();
	}
	public int PartIndex;


	void OnTriggerEnter(Collider other){

		if(other.tag=="PlayerBody" || other.tag=="Poison")
		{
//			int collidedSnakePartIndex=other.GetComponent<SnakeBodyScript>().PartIndex;
//			Debug.Log("head collided with a body part "+other.GetComponent<SnakeBodyScript>().PartIndex +" head index :"+PartIndex);
			//Destroy(other);
			Debug.Log("switching to new scene");
			snakePlayer.DieFull();
			StartCoroutine(WaitMethod());
			singletonScript.LoadNextScene();


		//	snakePlayer.deleteSnakeParts(collidedSnakePartIndex);
		//	gameManagerScript.updateScore(-10);
		}

	}
	IEnumerator WaitMethod(){
		Debug.Log("waiting");
		yield return new WaitForSeconds(10);
	}
}
