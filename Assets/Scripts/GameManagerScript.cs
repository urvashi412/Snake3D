﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameManagerScript : MonoBehaviour {

	public static int score=0;
	/*Initialisations*/
	public Text scoreText;

	void Start () {
		
	}
	
	public void updateScore(int scoreInc){
		if(scoreInc<0)
			score-=Mathf.Abs(scoreInc);
		else
			score+=scoreInc;
		
		scoreText.text=score.ToString();
	}
}
