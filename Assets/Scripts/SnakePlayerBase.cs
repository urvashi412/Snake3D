﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakePlayerBase : MonoBehaviour {
	public GameObject snakeBodyUnit;
	//	public GameObject snakeHeadUnit;
	public Mesh headMesh;
	public Mesh bodyMesh;
	public Mesh tailMesh;

	public Camera camera;
	// AudioSource itemEatingSoundSource;public AudioClip itemEatingSound;
	public Material bodyMaterial, headMaterial;
	public string direction;
	public float interval = 0.09f;
	public float nextTime = 0f;
	public List<GameObject> snakeList = new List<GameObject> ();
	public bool shouldDestroyTailBeforeMovingForward = true;
	public int noOfPartsToAdd = 0, tailsAdded = 0;
	public GameObject leftMaxOb,rightMaxOb,frontMaxOb,backMaxOb;
	public float leftMax,rightMax,frontMax,backMax;
	public Queue indexQueue = new Queue ();
	bool startGame=false;
	public GameObject headReference;
	void Awake(){
		noOfPartsToAdd=0;
	}
	SingletonScript singletonScript;
	Vector3 camRotation;
	private Vector3 velocity=Vector3.zero;
	public float smoothTime=0.3f;
	void Start ()
	{
		camRotation=new Vector3(65f,0,0);
		camera=GameObject.Find("Camera").GetComponent<Camera>();
		leftMax=leftMaxOb.transform.position.x;
		rightMax=rightMaxOb.transform.position.x;
		frontMax=frontMaxOb.transform.position.z;
		backMax=backMaxOb.transform.position.z;
		initialiseQueue ();
		singletonScript=GameObject.Find("Singleton").GetComponent<SingletonScript>();
		singletonScript.SetIntro();
		direction = "front";
		for (int i = 0; i < 5; i++) {
			Vector3 startVector = new Vector3 (0, 0, i);
			GameObject snakePart = (GameObject)Instantiate (snakeBodyUnit, startVector, Quaternion.Euler (new Vector3 (0, 90, 0)));
			snakeList.Insert (0, snakePart);
			snakePart.transform.parent = gameObject.transform;
		}
		snakeList [0].tag = "PlayerHead";
		headReference=snakeList [0];
		snakeList [0].GetComponent<MeshFilter> ().sharedMesh = headMesh;
		snakeList [0].GetComponent<Renderer> ().material = headMaterial;

		snakeList [snakeList.Count - 1].GetComponent<MeshFilter> ().sharedMesh = tailMesh;
		snakeList [snakeList.Count - 1].GetComponent<Renderer> ().material = headMaterial;
	}

	public virtual void initialiseQueue ()
	{
		for (int i = 0; i < 1000; i++) {
			indexQueue.Enqueue (i);
		}
	}

	public  void Update ()
	{
		if(Input.GetKey(KeyCode.Space)) {startGame=true;
			singletonScript.EndIntro();
			nextTime=Time.timeSinceLevelLoad+interval;
		}
		if(startGame)
		FrameWiseSnakeMove();

	}

	public virtual void LateUpdate(){
		Vector3 target=new Vector3(headReference.transform.position.x,camera.transform.position.y,headReference.transform.position.z-12f);
		iTween.MoveUpdate(camera.gameObject,target,8f);
		//camera.transform.position=target;
		//	camera.transform.position=Vector3.SmoothDamp(camera.transform.position,target,ref velocity,smoothTime);

		//camera.transform.position=Vector3.Lerp(camera.transform.position,target,Time.smoothDeltaTime*smoothTime);
	//	camera.transform.position=target;
	//	camera.transform.LookAt(headReference.transform);
	}

	public virtual void FrameWiseSnakeMove(){
		if (tailsAdded < noOfPartsToAdd) {
			shouldDestroyTailBeforeMovingForward = false;
		} else {
			noOfPartsToAdd=0;tailsAdded=0;
			shouldDestroyTailBeforeMovingForward = true;
		}
	
		getMovement ();
	}
	#region setting head-body-tail
	public void setIndexAsHead(int index){
		headReference=snakeList [index];
		snakeList [index].tag = "PlayerHead";
		snakeList [index].GetComponent<MeshFilter> ().sharedMesh = headMesh;
		snakeList [index].GetComponent<Renderer> ().material = headMaterial;
	}
	public void setIndexAsBody(int index){
		snakeList [index].tag = "PlayerBody";

		snakeList [index].GetComponent<MeshFilter> ().sharedMesh = bodyMesh;
		snakeList [index].GetComponent<Renderer> ().material = bodyMaterial;
	}
	public void setIndexAsTail(int index){
		headReference=null;
		snakeList [index].tag = "PlayerBody";
		snakeList [index].GetComponent<MeshFilter> ().sharedMesh = tailMesh;
		snakeList [index].GetComponent<Renderer> ().material = headMaterial;
	}
	#endregion
	public virtual  void Eat ()
	{
		GetComponent<AudioSource>().Play();
		//itemEatingSoundSource.PlayOneShot(itemEatingSound);
		Grow ();
	}

	public virtual void Grow ()
	{
		noOfPartsToAdd = Random.Range (2, 5);
		shouldDestroyTailBeforeMovingForward = true;
	}



	public   void Die ()
	{

		for (int i = snakeList.Count - 1; i >= 0; i--) {
			int freedIndex = snakeList [i].GetComponent<SnakeBodyScript> ().PartIndex;
			indexQueue.Enqueue (freedIndex);

		
			Destroy (snakeList [i]);
			snakeList.RemoveAt (i);
		}

		Start ();
	}
	public   void DieFull ()
	{

		for (int i = snakeList.Count - 1; i >= 0; i--) {
			
			iTween.FadeTo(snakeList[i],0,5);
			//Destroy (snakeList [i]);
			//snakeList.RemoveAt (i);
		}


	}
	public  void MoveToPosition (float x,float z)
	{

		moveBodyUnit(true,x,z);

	}
	public virtual void getMovement ()
	{
		if (Input.GetKeyDown (KeyCode.A)||Input.GetKeyDown (KeyCode.LeftArrow)) {
			if (direction != "left" && direction != "right") {
				direction = "left";
				moveBodyUnit (true,float.MaxValue,float.MaxValue);
			}

		} else if (Input.GetKeyDown (KeyCode.D)||Input.GetKeyDown (KeyCode.RightArrow)) {
			if (direction != "left" && direction != "right") {
				direction = "right";
				moveBodyUnit (true,float.MaxValue,float.MaxValue);
			}

		} else if (Input.GetKeyDown (KeyCode.W)||Input.GetKeyDown (KeyCode.UpArrow)) {
			if (direction != "front" && direction != "back") {
				direction = "front";
				moveBodyUnit (true,float.MaxValue,float.MaxValue);
			}

		} else if (Input.GetKeyDown (KeyCode.S)||Input.GetKeyDown (KeyCode.DownArrow)) {
			if (direction != "front" && direction != "back") {
				direction = "back";
				moveBodyUnit (true,float.MaxValue,float.MaxValue);
			}

		}
	
		if (Time.timeSinceLevelLoad > nextTime) {
			nextTime += interval;
			moveBodyUnit (shouldDestroyTailBeforeMovingForward,float.MaxValue,float.MaxValue);
			noOfPartsToAdd--;
		}
	}

	public virtual void moveBodyUnit (bool shouldDestroy,float acrossBorderX,float acrossBorderZ)
	{



	}

	public virtual void deleteSnakeParts (int fromIndex)
	{

		for (int i = snakeList.Count - 1; i >= 1; i--) {
			if (snakeList [i].GetComponent<SnakeBodyScript> ().PartIndex == fromIndex) {
				break;
			}
			indexQueue.Enqueue (snakeList [i].GetComponent<SnakeBodyScript> ().PartIndex);
			Destroy (snakeList [i]);
			snakeList.RemoveAt (i);
		}
	}

}
