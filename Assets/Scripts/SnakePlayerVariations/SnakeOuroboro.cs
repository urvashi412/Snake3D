﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeOuroboro : SnakePlayerBase {
	public bool reverseDir=false;
	public override void  FrameWiseSnakeMove(){
		if(reverseDir==true){
		snakeList.Reverse();
			for(int i=0;i<snakeList.Count;i++){
				snakeList[i].transform.Rotate(0f,180f,0f);
			}
			setIndexAsTail(snakeList.Count - 1);
			setIndexAsHead(0);



			float headRot=snakeList[0].transform.rotation.eulerAngles.y;

			if(headRot==-90||headRot==270)direction="back";
			else if(headRot>=90 && headRot<=92)direction="front";
			else if(headRot==180)direction="right";
			else if(headRot>=0 &&headRot<=2)direction="left";
			Debug.Log("headRot "+headRot+" direction "+direction);
			reverseDir=false;
		}

		if (tailsAdded < noOfPartsToAdd) {
			shouldDestroyTailBeforeMovingForward = false;
		} else {
			noOfPartsToAdd=0;tailsAdded=0;
			shouldDestroyTailBeforeMovingForward = true;
		}
		getMovement ();
	
		if(snakeList[0].transform.position.z>frontMax){
			MoveToPosition(float.MaxValue,backMax);

		}else if(snakeList[0].transform.position.z<backMax){
			MoveToPosition(float.MaxValue,frontMax);
		}else if(snakeList[0].transform.position.x<leftMax){
			MoveToPosition(rightMax,float.MaxValue);
		}else if(snakeList[0].transform.position.x>rightMax){
			MoveToPosition(leftMax,float.MaxValue);
		}

	}

//	public override void LateUpdate(){
//		Vector3 target=new Vector3(headReference.transform.position.x,camera.transform.position.y,headReference.transform.position.z-15f);
//		if(reverseDir){
//			Debug.Log("reverse dir true and in late update");
//		iTween.MoveUpdate(camera.gameObject,target,2f);
//
//		}
//		else{
//			camera.transform.position=target;
//		}
//
//	}

	public  override void moveBodyUnit (bool shouldDestroy,float acrossBorderX,float acrossBorderZ)
	{


		if (snakeList.Count > 0) {
			Vector3 frontNodesPos = snakeList [0].transform.position;

			/*shouldDrestroy will be true whenever the snake is moving in general. false when it has eaten something.
			 * */
			if (shouldDestroy == true) {
				GameObject.Destroy (snakeList [snakeList.Count - 1]);
				snakeList.Remove (snakeList [snakeList.Count - 1]);
			}
			GameObject snakePart = null;
			if (direction == "left") {
				if(acrossBorderX!=float.MaxValue)
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (acrossBorderX - 1.00001f, 0, frontNodesPos.z), Quaternion.Euler (new Vector3 (0, 0, 0)));
				else
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x - 1.00001f, 0, frontNodesPos.z), Quaternion.Euler (new Vector3 (0, 0, 0)));


			} else if (direction == "right") {
				if(acrossBorderX!=float.MaxValue)
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (acrossBorderX + 1.00001f, 0, frontNodesPos.z), Quaternion.Euler (new Vector3 (0, 180, 0)));
				else
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x + 1.00001f, 0, frontNodesPos.z), Quaternion.Euler (new Vector3 (0, 180, 0)));

			} else if (direction == "front") {
				if(acrossBorderZ!=float.MaxValue)
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x, 0, acrossBorderZ + 1.00001f), Quaternion.Euler (new Vector3 (0, 90, 0)));
				else
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x, 0, frontNodesPos.z + 1.00001f), Quaternion.Euler (new Vector3 (0, 90, 0)));

			} else if (direction == "back") {
				if(acrossBorderZ!=float.MaxValue)
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x, 0, acrossBorderZ- 1.00001f), Quaternion.Euler (new Vector3 (0, -90, 0)));
				else
					snakePart = (GameObject)Instantiate (snakeBodyUnit, new Vector3 (frontNodesPos.x, 0, frontNodesPos.z - 1.00001f), Quaternion.Euler (new Vector3 (0, -90, 0)));

			}
			snakePart.GetComponent<SnakeBodyScript> ().PartIndex = (int)indexQueue.Dequeue ();


			snakePart.transform.parent = gameObject.transform;
			snakeList.Insert (0, snakePart);


			setIndexAsTail(snakeList.Count - 1);
			setIndexAsBody(1);
			setIndexAsHead(0);

		}
		//		if(reverseDir==true){
		//			setIndexAsHead(snakeList.Count - 1);
		//			setIndexAsTail(0);
		//		}


	}

	public override  void Eat ()
	{
		GetComponent<AudioSource>().Play();
		reverseDir=true;

		Grow ();
	}
	public override void deleteSnakeParts (int fromIndex)
	{
		if(reverseDir==false){
			for (int i = snakeList.Count - 1; i >= 1; i--) {
				if (snakeList [i].GetComponent<SnakeBodyScript> ().PartIndex == fromIndex) {
					break;
				}
				indexQueue.Enqueue (snakeList [i].GetComponent<SnakeBodyScript> ().PartIndex);
				Destroy (snakeList [i]);
				snakeList.RemoveAt (i);
			}
		}
	}

}
