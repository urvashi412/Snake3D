﻿using UnityEngine;
using System.Collections;

public class FadingScript : MonoBehaviour {

	public Texture2D fadeTexture;
	public float fadeSpeed=0.06f;
	public int drawDepth=-1000;
	public int drawDepth2=-1;
	private float alpha=1.0f;
	private int fadeDirection=-1;
	bool fade=true;
	void OnGUI(){
		if(fade==true){
		alpha+=fadeDirection*fadeSpeed*Time.deltaTime;
		alpha=Mathf.Clamp01(alpha);
		GUI.color=new Color(GUI.color.r,GUI.color.g,GUI.color.b,alpha);
		GUI.depth=drawDepth;
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),fadeTexture);

		}else{
			GUI.color=new Color(GUI.color.r,GUI.color.g,GUI.color.b,alpha);
			GUI.depth=drawDepth2;
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),fadeTexture);
		}
	}
	
	public float StartFade(int dir){
		fadeDirection=dir;
		return fadeSpeed;
	}
	public void FadeForIntroduction(float alpha){
		Debug.Log("setting alpha");
		this.alpha=alpha;
		fade=false;
	}
//	void OnLevelWasLoaded(){
//		StartFade(-1);
//	}
}
