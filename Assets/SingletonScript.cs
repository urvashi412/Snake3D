﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class SingletonScript : MonoBehaviour {


	public static SingletonScript Instance;

	Hashtable introtable;

	Text sceneNameText;FadingScript fadingScript;
	GameObject introText;
	GameObject readyText;
	public bool gameStarted=true;
	GameManagerScript managerScript;
	int totalScore=0;
	bool once=false;bool gameover=false;
	void Start(){
		introtable=new Hashtable();
		fadingScript=GetComponent<FadingScript>();
		introtable.Add("Classic","The basic snake...");
		introtable.Add("Passage","The most artistic snake of them all.");
		introtable.Add("Ouroboro","When an Ouroboros eats its own tail, it just flips inside out and just keeps on truckin'. It's mildly horrifying to witness.");
		introtable.Add("Mamushi","Mamushis never think things through and have a bad habit of leaving enemies in their wake as a result.");
		introtable.Add("Cobra","Cobras seem to be very transparent and open snakes, but in reality they are just hiding their true intentions");

	}
	void Update(){
		if(gameover&& Input.GetKeyDown(KeyCode.Space)){
			PlayerPrefs.SetString("SceneToLoad","MenuScene");
			SceneManager.LoadSceneAsync("Loader");
			gameover=false;
		}
	}
	void Awake ()   
	{
		if (Instance == null)
		{
			
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(this.gameObject);
			return;

		}
		DontDestroyOnLoad(gameObject);

	}
	public void LoadNextScene(){
		EndGame();

	}
	public void EndGame(){
		
		gameStarted=false;
		introText=GameObject.Find("Canvas").transform.FindChild("Intro").gameObject;
		readyText=GameObject.Find("Canvas").transform.FindChild("Ready").gameObject;
		introText.SetActive(true);
		readyText.SetActive(true);
		introText.GetComponent<TextMeshProUGUI>().text="Game Over. Your total score: "+GameManagerScript.score;;
			readyText.GetComponent<TextMeshProUGUI>().text="Press space to go back";
	//	fadingScript.FadeForIntroduction(0.5f);
			Destroy(GameObject.Find("Snake"));
		gameover=true;
	}

	public void SetIntro(){
		//fadingScript.FadeForIntroduction(0.5f);
		gameStarted=false;
		introText=GameObject.Find("Canvas").transform.FindChild("Intro").gameObject;
		readyText=GameObject.Find("Canvas").transform.FindChild("Ready").gameObject;

		introText.SetActive(true);
		readyText.SetActive(true);
		string scene=PlayerPrefs.GetString("SceneToLoad");
		introText.GetComponent<TextMeshProUGUI>().text=introtable[scene].ToString();



	}

	public void EndIntro(){
		introText=GameObject.Find("Canvas").transform.FindChild("Intro").gameObject;
		readyText=GameObject.Find("Canvas").transform.FindChild("Ready").gameObject;

	//	fadingScript.FadeForIntroduction(0f);
		introText.SetActive(false);
		readyText.SetActive(false);
		gameStarted=true;
		//introText.text=intros[sceneId];

	}
//	IEnumerator FadeScreenAndLoadLevel(){
//		totalScore+=GameManagerScript.score;
//		float fadeTime=fadingScript.StartFade(1);
//		yield return new WaitForSeconds(fadeTime);
//		SceneManager.LoadSceneAsync(2);
//		fadeTime=fadingScript.StartFade(-1);
//	
//	}

}
